﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tongue : MonoBehaviour
{
    public Transform target;
    public Transform pivot;
    public Vector3 scale;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ScaleAround();
    }

    public void ScaleAround() {
         Transform pivotParent = pivot.parent;
         Vector3 pivotPos = pivot.position;
         pivot.parent = target;        
         target.localScale = scale;
         target.position += pivotPos - pivot.position;
         pivot.parent = pivotParent;
     }
}
