﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    Vector3 startPos, endPos;
    Vector3 camOffSet= new Vector3(0,0,10);
    GameObject sphere = null;
    Vector3 dir;   

    Camera camera;
    [SerializeField] LineRenderer lr;
    [SerializeField] AnimationCurve ac;
    [SerializeField] float maxLength;
    [SerializeField] GameObject tongue;


    void Start()
    {
        camera = Camera.main;
    }

    void Update()
    {
        Drag();
    }

    Vector3 Drag(){
           

        if(Input.GetMouseButtonDown(0)){
          lr.enabled = true;
          lr.positionCount = 2;
          startPos = camera.ScreenToWorldPoint(Input.mousePosition)+camOffSet;

          lr.SetPosition(0,transform.position);
          lr.useWorldSpace = true;
          lr.widthCurve = ac;
          lr.numCapVertices = 10;
        }
        if(Input.GetMouseButton(0)){
          endPos=camera.ScreenToWorldPoint(Input.mousePosition)+camOffSet-transform.position;
          dir = new Vector3(endPos.x-startPos.x, endPos.y-startPos.y);

          if(dir.magnitude > maxLength){
            dir = Vector3.Normalize(dir);
            dir *= maxLength;
          }

          lr.SetPosition(1, dir);            

          if(sphere != null)
            Destroy(sphere);

          sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
          sphere.transform.position = -dir;
          sphere.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
        }
        if(Input.GetMouseButtonUp(0)){
          lr.enabled = false;
          Destroy(sphere);
          Shoot(-dir);
        }

        return -dir;
    }

    void Shoot(Vector3 dir){
        float angle = Mathf.Asin(dir.y/dir.magnitude)*Mathf.Rad2Deg - 90;
        tongue.transform.rotation = Quaternion.Euler(0,0,angle);

        Debug.Log(angle);
    }

}
